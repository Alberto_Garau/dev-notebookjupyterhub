# Immagini PythonNotebook destinate alla piattaforma JupyterHub

La procedura sotto descritta spiega come gestire le immagini con librerie personalizzate.

1. Fare la build e testare il corretto funzionamento del notebook. 
```bash
$: # Build and test
$: docker build  --rm -t albertogarau/devjhubnotebook:pyspark320 . 
$: docker run -p 8888:8888 albertogarau/devjhubnotebook:tensorflow00011
$: # with sudo:
$: docker run -e GRANT_SUDO=yes --user root -p 8888:8888 albertogarau/devjhubnotebook:pyspark320
$: # with shell in local
$: docker run -d -v $PWD:/home/jovyan/work -e GRANT_SUDO=yes --user root -p 8888:8888 albertogarau/devjhubnotebook:scienze00002
$: sudo docker exec -it frosty_dhawan bash
```

Nel notebook si entra tramite:
> http://127.0.0.1:8888/?token=6b42dd960e34a60fc7ae7fd39eefbb6e1fd9de1467e280dd

2. Fare il push dell'immagine nel repository git del docker presente su root.

3. Digitare:
```bash
jupyter-repo2docker \
    --no-run \
    --user-name=jovyan \
    --image=albertogarau/devjhubnotebook:pyspark320 \
    https://Alberto_Garau@bitbucket.org/Alberto_Garau/dev-notebookjupyterhub.git
```

4. Effettuare il login è fare il push su repository docker.

```bash
$: # This not
$: docker login
$: docker push albertogarau/devjhubnotebook:tensorflow00011
```
