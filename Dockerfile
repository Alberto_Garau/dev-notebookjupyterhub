FROM jupyter/pyspark-notebook:spark-3.2.0

# install additional package...
COPY requirements.txt ./
RUN pip install --no-cache-dir astropy
USER root
RUN apt-get update && apt-get install -y libspatialindex-dev
RUN apt-get install python3-icu -y
#RUN apt-get install python-pydot python-pydot-ng graphviz -y
RUN apt-get install graphviz -y
#RUN pip install pystan==2.19.1.1
RUN pip install -r requirements.txt
RUN rm ./requirements.txt



#! For luca Lesson
RUN apt-get install openjdk-8-jdk-headless -qq > /dev/null
RUN pip install -q findspark \
    pip install pyarrow      \
    pip install pyspark[sql]==3.2.0
###> It works: https://github.com/jupyter/docker-stacks/issues/815
# Add systemwide .bashrc configuration additions that are normally found in /home/jovyan/.bashrc
#USER root
RUN cat /etc/skel/.bashrc >> /etc/bash.bashrc 
USER $NB_UID
##


# RUN mkdir test
# COPY 06.pandas_udf.ipynb test/