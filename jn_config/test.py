from pyspark.sql import SparkSession
spark = SparkSession.builder.appName("DatalakeManagement").getOrCreate()
import urllib.request
import json
import os


s3_data_url = "s3a://datalake.turismo.dev/intake/facebook/page-timeline/year_p=2018/month_p=07/day_p=30/page_name_p=CharmingSardinia/facebook_timeline_CharmingSardinia_1532939094.json.gz"
df = spark.read.json(s3_data_url)
